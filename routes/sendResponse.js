/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var constant = require('./constant');

exports.parameterMissingError = function (res) {

    var errResponse = {
        status: constant.responseStatus.PARAMETER_MISSING,
        message: constant.responseMessage.PARAMETER_MISSING,
        data: {}
    }
    sendData(errResponse,res);
};

exports.emailNotRegistered = function (res) {

    var errResponse = {
        status: constant.responseStatus.EMAIL_NOT_REGISTERED,
        message: constant.responseMessage.EMAIL_NOT_REGISTERED,
        data: {}
    }
    sendData(errResponse,res);
};

exports.passwordIncorrect = function (res) {

    var errResponse = {
        status: constant.responseStatus.PASSWORD_INCORRECT,
        message: constant.responseMessage.PASSWORD_INCORRECT,
        data: {}
    }
    sendData(errResponse,res);
};

exports.emailExists = function (res) {

    var errResponse = {
        status: constant.responseStatus.EMAIL_EXISTS,
        message: constant.responseMessage.EMAIL_EXISTS,
        data: {}
    }
    sendData(errResponse,res);
};

exports.alreadyLoggedIn = function (res) {

    var errResponse = {
        status: constant.responseStatus.LOGGED_IN,
        message: constant.responseMessage.LOGGED_IN,
        data: {}
    }
    sendData(errResponse,res);
};

exports.alreadyLoggedout = function (res) {

    var errResponse = {
        status: constant.responseStatus.LOGGED_OUT,
        message: constant.responseMessage.LOGGED_OUT,
        data: {}
    }
    sendData(errResponse,res);
};

exports.sendError = function (msg,res) {

    var errResponse = {
        status: constant.responseStatus.SHOW_ERROR_MESSAGE,
        message: msg,
        data: {}
    };
    sendData(errResponse,res);
};
exports.somethingWentWrongError = function (res) {

    var errResponse = {
        status: constant.responseStatus.ERROR_IN_EXECUTION,
        message: constant.responseMessage.ERROR_IN_EXECUTION,
        data: {}
    }
    sendData(errResponse,res);
};
exports.sendSuccessData = function (data,res) {

    var successResponse = {
        status: constant.responseStatus.SHOW_DATA,
        message: constant.responseMessage.SUCCESSFUL_EXECUTION,
        data: data
    };
    sendData(successResponse,res);
};
exports.sendSuccessLogout = function (res) {

    var successResponse = {
        status: constant.responseStatus.SUCCESSFUL_LOGOUT,
        message: constant.responseMessage.SUCCESSFUL_LOGOUT,
        data: {}
    };
    sendData(successResponse,res);
};
exports.sendSuccessReport = function (data,res) {

    var successResponse = {
        status: constant.responseStatus.SUCCESSFUL_LOGGING,
        message: constant.responseMessage.SUCCESSFUL_LOGGING,
        data: data
    };
    sendData(successResponse,res);
};

exports.sendData = function (data,res) {
    sendData(data,res);
};


function sendData(data,res)
{
    res.type('json');
    res.jsonp(data);
}