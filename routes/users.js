var express = require('express');
var router = express.Router();
var func= require('./commonFunction');
var sendResponse = require('./sendResponse');
var generatePassword = require('password-generator');
var moment = require('moment');
var md5 = require('MD5');
var math = require('math');
var async = require('async');
var request = require("request");


/* Sign_Up Panel */
router.post('/sign_up', function(req, res,next) {
    var firstName = req.body.first_name;
    var lastName = req.body.last_name;
    var phoneNo = req.body.phone_no;
    var email = req.body.email;
    var password = req.body.password;
    
    var checkBlank = [firstName,lastName,phoneNo,email,password];
      console.log(checkBlank);
    async.waterfall([
        function(callback) {
            func.checkBlankWithCallback(res, checkBlank, callback);

        },
        function(callback) {
            func.checkEmailAvailability(res, email, callback);

        }], function(updatePopup) {

                var hash = md5(password);
                var accesstoken = func.generateRandomString();
                var accesstoken1 = accesstoken + email;
                var accessToken = md5(accesstoken1);
                var loginTime = new Date();
                var sql = "INSERT into tb_users(user_email,password,access_token,date_registered,last_login,first_name,last_name,phone_no) \n\
                    values(?,?,?,?,?,?,?,?)";
                connection.query(sql, [email, hash, accessToken, loginTime, loginTime,firstName,lastName, phoneNo], function(err, result) {

                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }

                    else
                    {
                        var sql = "SELECT `first_name`,`last_name`,`access_token`,`phone_no`,`user_id`FROM \n\
                       `tb_users` WHERE `user_email`=? LIMIT 1";
                        connection.query(sql, [email], function(err, user) {
                            var final = {"first_name": user[0].first_name, "last_name": user[0].last_name,"access_token": user[0].access_token,
                                "email": email, "phone_no": user[0].phone_no};


                            sendResponse.sendSuccessData(final,res);
                            var userName = user[0].first_name;
                            func.sendMailToCustomer(userName, email);

                        });
                    }

                });
            
        });
    });

/*Login Panel*/

router.post('/login', function(req, res, next) {
  var email = req.body.email;
    var password = req.body.password;
    var checkBlank = [email,password];
    console.log(checkBlank);
    async.waterfall(
            [function(callback) {
                    func.checkBlankWithCallback(res, checkBlank, callback);
                }, ], function(updatePopup) {

        func.email_login_function(email, password, res);
        
    });
});

router.post('/logout',function(req,res,next){
    
    var email=req.body.email;
       
    var sql="SELECT `login_status` FROM `tb_users` WHERE `user_email`=? LIMIT 1"
    connection.query(sql,[email],function(err,results){
        if(err)
        {
            sendResponse.somethingWentWrongError(res);
        }
        else if(results[0].login_status == 1)
        {
          var sql="UPDATE `tb_users` SET `login_status`=? WHERE `user_email`=? LIMIT 1"
          connection.query(sql,[0,email],function(err,result){
        if(err)
        {
            sendResponse.somethingWentWrongError(res);
        }
        else 
        {
            sendResponse.sendSuccessLogout(res);
        }
    });   
        }
        else if(results[0].login_status == 0)
        {
            sendResponse.alreadyLoggedout(res);
        }
    });
});
module.exports = router;

