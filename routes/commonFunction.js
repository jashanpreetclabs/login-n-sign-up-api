/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var sendResponse = require('./sendResponse');
var config = require('config');

exports.checkBlankWithCallback = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
}

function checkBlank(arr) {
    var arrlength = arr.length;
    for (var i = 0; i < arrlength; i++) {
        if (arr[i] == '' || arr[i] == undefined || arr[i] == '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}

exports.checkEmailAvailability = function (res, email, callback) {

    var sql = "SELECT `user_id` FROM `tb_users` WHERE `user_email`=? limit 1";
    connection.query(sql, [email], function (err, response) {

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else if (response.length) {
            sendResponse.emailExists(res);
        }
        else {
            callback();
        }
    });
};

exports.generateRandomString = function ()
{
    var math = require('math');

    var text = "";
    var possible = "0123456789";

    for (var i = 0; i < 4; i++)
        text += possible.charAt(math.floor(math.random() * possible.length));

    return text;
};
function generateRandomString()
{
    var math = require('math');

    var text = "";
    var possible = "0123456789";

    for (var i = 0; i < 4; i++)
        text += possible.charAt(math.floor(math.random() * possible.length));

    return text;
};

exports.sendMailToCustomer = function (userName, email)
{
    
    var toEmail = email;
    var sub = "Welcome!!";
    var html = '<html>';
    html += '<head>';
    html += '<title></title>';
    html += '</head>';
    html += '<body>';
    html += 'Welcome';
    html += '<p>Dear ' + userName + ',</p>';
    html += '<p>Hello.</p>';
    html += '<p>Please do not reply to this email. Emails sent to this address will not be answered.</p>';
    html += '<br/>';
    html += '<span>Copyright &copy; 2015 Jashanpreet Singh, LLC. All rights reserved.</span>';
    html += '</body>';
    html += '</html>';
    sendEmail(toEmail, html, sub, function (result)
    {
        if (result === 1)
        {
            console.log("Mail Sent");

        }
        else
        {
            console.log("Error Sending Mail");
        }
    });
}

function sendEmail(receiverMailId, message, subject, callback) {
    
    var nodemailer = require('nodemailer');
    var smtpTransport = nodemailer.createTransport("SMTP", {
        service: "Gmail",
        auth: {
            user: config.get('emailSettings').email,
            pass: config.get('emailSettings').password
        }
    });

// setup e-mail data with unicode symbols
    var mailOptions = {
        from: config.get('emailSettings').email,
        to: receiverMailId,
        subject: subject,
        text: message
        //html: "<b>Hello world ?</b>" // html body
    };

// send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function (error, response) {
        
        if (error)
        {
            return callback(0);
            
        } else
        {
            //  console.log('hi');
            return callback(1);
        }

        // if you don't want to use this transport object anymore, uncomment following line
        //smtpTransport.close(); // shut down the connection pool, no more messages
    });
};


exports.email_login_function = function (email, password, res)
{
    var sql="SELECT `login_status` FROM `tb_users` WHERE `user_email`=? LIMIT 1"
    connection.query(sql,[email],function (err,result){
        
        if(err)
        {
            sendResponse.somethingWentWrongError(res);
        }
        else if(result[0].login_status === 1 )
        {
            sendResponse.alreadyLoggedIn(res);
        }
        else
        {
            var md5 = require('MD5');
    var hash = md5(password);
    var sql = "SELECT `user_id` FROM `tb_users` WHERE `user_email`=? LIMIT 1"
    connection.query(sql, [email], function (err, result)
    {
        if (err)
        {
            sendResponse.somethingWentWrongError(res);
        }
        else
        {
            var length2 = result.length;
            if (length2 == 0)
            {
                sendResponse.emailNotRegistered(res);
            }
            else
            {
                var user = result[0].user_id;

                var sql = "SELECT `access_token`,`first_name`,`phone_no` FROM `tb_users` WHERE `user_id`=? && `password`=? LIMIT 1"
                connection.query(sql, [user, hash], function (err, result_user)
                {
                    if (err)
                    {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else
                    {
                        
                        var result_user_length = result_user.length;
                        if (result_user_length === 0)
                        {
                            sendResponse.passwordIncorrect(res);

                        }
                        else
                        {
                            var loginTime = new Date();
                            var accesstoken = generateRandomString();
                            var accesstoken1 = accesstoken + email;
                            var accessToken = md5(accesstoken1);
                            var sql = "UPDATE `tb_users` SET `last_login`=? ,`login_status`=? ,`access_token` = ?  WHERE `user_id`=? LIMIT 1";
                            connection.query(sql, [loginTime,1,accessToken,user], function (err, result) {
                                if (err) {
                                    
                                    sendResponse.somethingWentWrongError(res);
                                }

                                else
                                {
                                    console.log(result);
                                    sendResponse.sendSuccessReport(result_user, res);
                                }
                            });


                        }

                    }

                });
            }
        }
    });
            
        }
    });
    
}